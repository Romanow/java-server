package org.example.server.model;

import lombok.Data;

/**
 * Created by ronin on 13.09.2017.
 */
@Data
public class ItemRequest {
    private String description;
    private Integer price;
}
